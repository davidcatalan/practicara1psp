import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Created by davidcatalanlisa on 28/11/16.
 */
public class Aplicacion {

    private static final Logger logger = LogManager.getLogger(Aplicacion.class);

    public static void main (String args[]){
        VentanaDescargas ventana = new VentanaDescargas();
        logger.trace("Se ha iniciado la aplicacion");
    }
}
