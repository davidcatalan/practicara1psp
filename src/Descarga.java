import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by davidcatalanlisa on 28/11/16.
 */
public class Descarga extends SwingWorker<Void, Integer>{

    // La URL del fichero a descargar
    private String urlFichero;
    // La ruta del disco donde se descargará
    private String rutaFichero;

    private static final Logger logger = LogManager.getLogger(Descarga.class);
    /*
    http://www.jc-mouse.net/java/anadir-splash-screen-en-javanetbeans
     */
    public Descarga(String urlFichero, String rutaFichero) {
        this.urlFichero = urlFichero;
        this.rutaFichero = rutaFichero;
    }


    @Override
    protected Void doInBackground() throws Exception {
        URL url = new URL(urlFichero);
        URLConnection conexion = url.openConnection();

        //Obtiene el tamaño del fichero en bytes
        int tamanoFichero = conexion.getContentLength();

        firePropertyChange("tamanoFichero", 0, tamanoFichero);

        InputStream is = url.openStream();
        FileOutputStream fos = new FileOutputStream(rutaFichero);
        byte[] bytes = new byte[8192];
        int longitud = 0;
        int progresoDescarga = 0;
        long tiempoDescarga = 0;
        int longitudAcumulada = 0;

        tiempoDescarga = System.currentTimeMillis();

        // Descarga el fichero y va notificando el progreso
        while ((longitud = is.read(bytes)) != -1) {

            if ((System.currentTimeMillis() - tiempoDescarga) > 500) {
                float tiempoTranscurrido =
                        ((float) (System.currentTimeMillis() - tiempoDescarga)) / 1000;
                float velocidad = longitudAcumulada / tiempoTranscurrido;
                firePropertyChange("velocidadDescarga", 0, velocidad);
                tiempoDescarga = System.currentTimeMillis();
                longitudAcumulada = 0;
            }
            longitudAcumulada += longitud;



            fos.write(bytes, 0, longitud);

            progresoDescarga += longitud;
            setProgress((int) (progresoDescarga * 100 / tamanoFichero));
            firePropertyChange("descarga", 0, progresoDescarga);
        }

        is.close();
        fos.close();

        setProgress(100);
        logger.trace("La descarga a finalizado");

        return null;
    }
}
