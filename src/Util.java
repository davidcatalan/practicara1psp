import java.text.DecimalFormat;

/**
 * Created by davidcatalanlisa on 11/12/16.
 */
public class Util {

    public static String pasarAMegas(float numeroBytes) {

        DecimalFormat df = new DecimalFormat("#,## MB 0.00");
        return df.format((float) numeroBytes / (1024 * 1024));
    }

    public static String pasarAMegasSegundo(float numeroBytesSegundo) {

        DecimalFormat df = new DecimalFormat("#,## MB/s 0.00");
        return df.format(numeroBytesSegundo / (1024 * 1024));
    }

    public static String nombreArchivo(String nombreUrl){
        String[] nUrlSplit = nombreUrl.split("/");
        String nArch = "";
        for (int i = 0; i < nUrlSplit.length; i++){
            if (i == nUrlSplit.length -1) nArch = nUrlSplit[i];
        }
        return nArch;

    }

}
