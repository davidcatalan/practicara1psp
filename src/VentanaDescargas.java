import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


/**
 * Created by davidcatalanlisa on 28/11/16.
 */
public class VentanaDescargas implements ActionListener, KeyListener{
    /*
    https://i.ytimg.com/vi/a-GjpVGW9lw/hqdefault.jpg
     */
    JTextField tfAñadirTitulo;
    JButton btAñadir;
    JPanel panelAñadirDes;
    JScrollPane spPaneles;
    JPanel paneles;

    JMenuItem menuItemAbrir;
    JMenuItem menuItemCerrar;

    private static final Logger logger = LogManager.getLogger(VentanaDescargas.class);

    public VentanaDescargas(){
        JFrame frame = new JFrame("VentanaDescargas");
        frame.setContentPane(panelAñadirDes);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        //frame.setJMenuBar(getMenuBar());
        frame.pack();
        frame.setVisible(true);

        addActionListener();
    }

    /**
            PARA LOS JMENUUUUS
     */

    /*
    private JMenuBar getMenuBar(){
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenu menu2 = new JMenu("Acerca de...");
        menuBar.add(menu);
        menuBar.add(menu2);
        menuItemAbrir = new JMenuItem("Abrir");
        menuItemCerrar = new JMenuItem("Cerrar");
        menu.add(menuItemAbrir);
        menu.add(menuItemCerrar);

        return menuBar;
    }
    */
    private void addActionListener() {
        btAñadir.addActionListener(this);
        btAñadir.addKeyListener(this);
        //menuItemCerrar.addActionListener(this);
        //menuItemAbrir.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.trace("Se ha añadido una descarga");
        PanelDescarga pDescargas = new PanelDescarga();
        paneles.add(pDescargas.panel);
        tfAñadirTitulo.setText("");
        paneles.revalidate();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        //SIRVE PARA PCUSTOMIZAR LO QUE QUIERAS
        paneles = new JPanel();
        paneles.setLayout(new BoxLayout(paneles, BoxLayout.Y_AXIS));
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            btAñadir.doClick();
        }

        logger.trace("Se ha pulsado el boton enter");
    }
}


