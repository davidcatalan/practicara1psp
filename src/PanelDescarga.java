import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;


/**
 * Created by davidcatalanlisa on 28/11/16.
 */
public class PanelDescarga extends JFrame implements ActionListener{
    JTextField tfDescarga;
    JProgressBar pbDescarga;
    JButton btIniciar;
    JButton btCancelar;
    JButton btCambiar;
    JPanel panel;
    JLabel lbMegas;
    JLabel lbVelocidad;

    PanelDescarga panelDes;

    String rFichero="";
    String urlFichero;
    Descarga descarga = null;
    private int tamanoFichero;

    private static final Logger logger = LogManager.getLogger(PanelDescarga.class);

    public PanelDescarga(){
        addActionListener();
    }

    private void addActionListener() {
        btIniciar.addActionListener(this);
        btCancelar.addActionListener(this);
        btCambiar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Iniciar":
                logger.trace("Pulsado el boton iniciar descarga, se inicia la descarga");
                descargarFichero();
                break;
            case"Cancelar":
                // como puedo cerrar aqui la ventana???
                logger.trace("Pulsado el botono cancelar y se  elimina la descarga");
                descarga.isCancelled();
                panel.setVisible(false);
                break;
            case "Cambiar Ruta":
                logger.trace("Pulsado el boton cambiar Ruta");
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.showOpenDialog(panel);
                urlFichero= tfDescarga.getText();
                rFichero=chooser.getSelectedFile().getAbsolutePath()+ File.separator + Util.nombreArchivo(urlFichero);
                break;
        }
    }

    private void descargarFichero() {
        if(tfDescarga.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Error de URL", "Introduce la URL de descarga", JOptionPane.ERROR_MESSAGE);
            return;
        }
        urlFichero= tfDescarga.getText();
        if(rFichero.equals("")){
            rFichero=System.getProperty("user.home")+ File.separator+"Desktop" + File.separator + Util.nombreArchivo(urlFichero);
        }
        try {
            descarga = new Descarga(tfDescarga.getText(), rFichero);
            descarga.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent event) {
                    switch (event.getPropertyName()){
                        case"progress":
                            pbDescarga.setValue((Integer) event.getNewValue());
                            break;
                        case"isCancelled":
                            break;
                        case"descarga":
                            lbMegas.setText(Util.pasarAMegas((Integer) event.getNewValue())
                                    + " / " + Util.pasarAMegas(tamanoFichero));
                            break;
                        case"tamanoFichero":
                            tamanoFichero = (int) event.getNewValue();
                            break;
                        case "velocidadDescarga":
                            lbVelocidad.setText(Util.pasarAMegasSegundo((float) event.getNewValue()));
                            break;
                    }
                }
            });
            descarga.execute();
            logger.trace("La descarga se ha ejecutado correctamente");

        } catch (Exception e) {
            if (e instanceof MalformedURLException) {
                JOptionPane.showMessageDialog(null, "La URL no es correcta", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                logger.error("La url no es correcta");
            }else if (e instanceof FileNotFoundException) {
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                logger.error("No se ha podido leer el fichero");
            }else {
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                logger.error("No se puede leer el fihero");
            }
            e.printStackTrace();
        }
        }
}
